#!/usr/bin/python3
# ===============================================
# File: update_rhsa_doc.py 
# Author: Gabriela Necasova 
# Email: gnecasov@redhat.com
# Date: June 2024
# Description: This script generates documentation texts for given RHSA advisories specified by URL filter. 
# In documentation process, we modify these fields: Synopsis, Topic, Problem Description, Solution and References.
# Note: This work is based on the errata-text.py script provided in https://issues.redhat.com/browse/OSCI-6533
# and also errata-automation project: https://gitlab.cee.redhat.com/osci/errata-automation.    
# ===============================================

import argparse
import json
import sys
import re
import requests
import logging
import koji # https://pypi.org/project/koji/ 
from pprint import pformat
from urllib.parse import quote
from errata_tool import ErrataConnector, Erratum
from requests_kerberos import HTTPKerberosAuth
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtplib import SMTP

RE_CVE = re.compile(r'^CVE-\d{4}-\d{4,}$')
RE_FLAW_SUMMARY = re.compile(r'^(?:EMBARGOED )?(?:\(?CVE-\d{4}-\d{4,}(?:\s?,?\s+CVE-\d{4}-\d{4,})*\)? )?(.*)$')

CA_CERTS = '/etc/pki/tls/certs/ca-bundle.crt'

# Note: There are two possible BASE_URLs and there is no difference between them:
# 'https://errata.engineering.redhat.com/'
# 'https://errata.devel.redhat.com/'
BASE_URL = 'https://errata.devel.redhat.com/'
ADVISORY_URL = BASE_URL + '/api/v1/erratum/{id}'
ADVISORY_URL_ET = '/api/v1/erratum/'
SHOW_ADVISORY_URL = 'https://errata.devel.redhat.com/docs/show/'
DETAILS_ADVISORY_URL = 'https://errata.devel.redhat.com/advisory/details/'
ERRATA_PV_URL = (
    "https://errata.devel.redhat.com/api/v1/"
    "products/RHEL/product_versions.json?filter[name]={pv_name}"
)
ADVISORY_PACKAGE_URL = "https://errata.devel.redhat.com/api/v1/packages/?name="
BREW_URL = "https://brewhub.engineering.redhat.com/brewhub"

# RHSA filter: https://errata.devel.redhat.com/advisory/filters/4051   
# (RHSA; State REL PREP; Product: LACD, RHEL, RHEL-EXTRAS; docs not requested, in queue (requested), in queue (needs redraft); sorted by release date (earliest) then by newest.
# https://errata.devel.redhat.com/api/v1/erratum/search?doc_status%5B%5D=not_reqd&doc_status%5B%5D=requested&doc_status%5B%5D=need_redraft&hotfix_option=&prerelease_option=&product%5B%5D=23&product%5B%5D=16&product%5B%5D=102&show_state_REL_PREP=1&show_type_RHSA=1&sort_by_fields%5B%5D=reldate&sort_by_fields%5B%5D=new&page=1
RHSA_FILTER_URL_BK = (
    "https://errata.devel.redhat.com/api/v1/erratum/search?"
    "doc_status%5B%5D=not_reqd&doc_status%5B%5D=requested&doc_status%5B%5D=need_redraft&hotfix_option=&prerelease_option=&"
    "product%5B%5D=23&product%5B%5D=16&product%5B%5D=102&"
    "show_state_REL_PREP=1&show_type_RHSA=1&sort_by_fields%5B%5D=reldate&sort_by_fields%5B%5D=new&page={page}"
)

# WITHOUT LACD!
# RHSA filter: https://errata.devel.redhat.com/advisory/filters/4051   
# (RHSA; State REL PREP; Product: RHEL, RHEL-EXTRAS; docs not requested, in queue (requested), in queue (needs redraft); sorted by release date (earliest) then by newest.
# https://errata.devel.redhat.com/api/v1/erratum/search?doc_status%5B%5D=not_reqd&doc_status%5B%5D=requested&doc_status%5B%5D=need_redraft&hotfix_option=&prerelease_option=&product%5B%5D=16&product%5B%5D=102&show_state_REL_PREP=1&show_type_RHSA=1&sort_by_fields%5B%5D=reldate&sort_by_fields%5B%5D=new&page=1
RHSA_FILTER_URL = (
    "https://errata.devel.redhat.com/api/v1/erratum/search?"
    "doc_status%5B%5D=not_reqd&doc_status%5B%5D=requested&doc_status%5B%5D=need_redraft&hotfix_option=&prerelease_option=&"
    "product%5B%5D=16&product%5B%5D=102&"
    "show_state_REL_PREP=1&show_type_RHSA=1&sort_by_fields%5B%5D=reldate&sort_by_fields%5B%5D=new&page={page}"
)

# email settings
SMTP_SERVER = "smtp.redhat.com"

# TODO: sender should be a bot, no-reply email
SENDER_EMAILS = "gnecasov@redhat.com"
DOCS_EMAILS = ["gnecasov@redhat.com", "lspackova@redhat.com"]

logger = logging.getLogger()


def set_logger(level: str):
    formatter = logging.Formatter('%(levelname)s: %(message)s')
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    log_mode = logging.ERROR  # default level
    if level == 'debug':
        log_mode = logging.DEBUG
    elif level == 'info':
        log_mode = logging.INFO
    elif level == 'warning':
        log_mode = logging.WARNING

    logger.setLevel(log_mode)


def get_args():
    parser = argparse.ArgumentParser(description="Generates documentation texts for RHSA advisories.\n \
                                    In documentation process, we modify these fields: Synopsis, Topic, Problem Description, Solution and References.")
    
    # define the optional --advisory argument with a value
    parser.add_argument("-a", "--advisory", type=int, help="ID of the advisory")

    # define the optional --readonly flag
    parser.add_argument("-r", "--readonly", action="store_true", help="No communication with the server")

    # logging level
    parser.add_argument("-l", "--level", type=str, default="warning", help="set logging level: error, warning, info, debug")
    
    # define the optional --email flag
    parser.add_argument("-e", "--email", action="store_true", help="Send email to PO and QA if error occurs")

    args = parser.parse_args()

    return parser.parse_args()


def comma_string_from_list(string_list: list) -> str:
        if len(string_list) == 1:
            return string_list[0]
        elif len(string_list) == 2:
            return " and ".join(string_list)
        else:
            return ", ".join(string_list[:-1]) + f", and {string_list[-1]}"

# ------------------------------
# class definition
# ------------------------------
class ErrataUpdater:
    def __init__(self):
        self.errata_json = None
        self.errata = None
        self.errata_package = None
        
        self.errata_flaws = list()
        # TODO: not needed in case of RHSAs, might be implemented later
        self.errata_bugfixes = list()
        self.errata_enhancements = list()

        self.product_versions = dict()
        
        self.brew_session = koji.ClientSession(BREW_URL)
        self.brew_session.gssapi_login()

        self.errata_ids = list()
        self.errata_cve_names = list()
        self.data = dict()

        self.readonly = True
        self.email = False
        self.empty_bugs = False


    def print_errata(self):
        logger.debug(f"""\n{'='*50}
        ADVISORY ID: {self.errata.errata_id}
        ADVISORY URL: {self.errata.url()}
        SYNOPSIS: {self.errata.synopsis}
        TOPIC: {self.errata.topic}
        DESCRIPTION: {self.errata.description}
        SOLUTION: {self.errata.solution}
        {'-'*50}\n""")


    # TODO: Not used now, we are using ErrataConnector() instead
    def put_advisory_info(self, data: dict) -> int:
        # DOC: https://errata.devel.redhat.com/documentation/developer-guide/api-http-api.html#advisories
        return requests.put(ADVISORY_URL.format(self.errata.errata_id), data=data, auth=HTTPKerberosAuth(), verify=CA_CERTS)


    def get_advisory_info(self):
        # DOC: https://errata.devel.redhat.com/documentation/developer-guide/api-http-api.html#advisories
        return requests.get(ADVISORY_URL.format(self.errata.errata_id), auth=HTTPKerberosAuth(), verify=CA_CERTS).json()
    

    def get_rhsa_advisories(self) -> list:
        url = RHSA_FILTER_URL.format(page=1) 
        advisories_data = ErrataConnector()._get(url)
        # extract the advisory IDs
        advisory_ids = [e["id"] for e in advisories_data["data"]]

        num_pages = advisories_data["page"]["total_pages"]

        if num_pages > 1:
            for i in range(2, num_pages+1):
                url = RHSA_FILTER_URL.format(page=i) 
                advisories_data = ErrataConnector()._get(url)
                # extract the advisory IDs
                advisory_ids.extend([e["id"] for e in advisories_data["data"]])
        
        logger.info(f"NUMBER OF ADVISORIES: {len(advisory_ids)}")
        return advisory_ids


    def get_package_description(self) -> str:
        # preprocessing - remove digits and dots from the package name
        # Example 1: python3.11: https://errata.devel.redhat.com/advisory/details/133323
        #   the correct url is: https://errata.devel.redhat.com/api/v1/packages/?name=python, 
        #   not https://errata.devel.redhat.com/api/v1/packages/?name=python3.11
       
        # remove version only if it is not a "pythonX.Y-PyMySQL" package name
        if "PyMySQL" not in self.errata_package:
            match = re.match(r'^([^\.]+)', self.errata_package)
            if match:
                self.errata_package = match.group(1)

        url = f"{ADVISORY_PACKAGE_URL}{self.errata_package}"
        logger.info(f"PACKAGE URL: {url}")

        pkg = ErrataConnector()._get(url)["data"]["attributes"]["description"]
        if pkg:
            return pkg + "\n\n"

        logger.info(f"ET BUILD VALUES: {self.errata.errata_builds.values()}")
        a_build = next(iter(self.errata.errata_builds.values()))[0]
        src_rpm = [
            x for x in self.brew_session.listBuildRPMs(a_build) if x["arch"] == "src"
        ]
        return (
            self.brew_session.getRPMHeaders(
                rpmID=src_rpm[0]["id"], headers=["description"]
            )["description"].replace("\n", " ")
            + "\n\n"
        )


    def get_synopsis(self) -> str:
        synopsis = f"{self.errata.security_impact}: {self.errata_package} security update"
        return synopsis
    

    def get_topic(self) -> str:
        topic = (
            f"An update for {self.errata_package} is now available "
            f"for {self.get_product_version_str(self.errata.errata_builds.keys())}."
        )

        topic = (
            f"{topic}\n\n"
            f"Red Hat Product Security has rated this update as having a security impact "
            f"of {self.errata.security_impact}. A Common Vulnerability Scoring "
            f"System (CVSS) base score, which gives a detailed severity rating, is "
            f"available for each vulnerability from the CVE link(s) in the References "
            "section."
        )

        return topic 


    def get_product_version_str(self, pvs: dict) -> dict:
        product_versions = dict()
        pv_descriptions = []
        for pv_name in pvs:
            url = ERRATA_PV_URL.format(pv_name=quote(pv_name))
            r = ErrataConnector()._get(url)
            if len(r["data"]) != 1:
                raise AttributeError(
                    f"Failed to get single product version with name {pv_name}"
                )
            pv_descriptions.append(r["data"][0]["attributes"]["description"])
            self.product_versions[tuple(pvs)] = comma_string_from_list(pv_descriptions)
        return self.product_versions[tuple(pvs)]


    def get_problem_description(self) -> str:   
        description = ""
        pkg_description = self.get_package_description()
        description += pkg_description

        if not pkg_description:
            raise AttributeError(
                f"Failed to get package description for {self.errata_package}"
            )

        if not self.load_bugs():
            return ""

        self.errata_flaws.sort(key=self.flaw_key)

        if self.empty_bugs:
            logger.info("Skipping errata with no required-doc-text bugs")

        description += self.generate_bug_list()

        return description


    def get_solution(self) -> str:
        solution = (
            "For details on how to apply this update, which includes the changes "
            "described in this advisory, refer to:\n\n"
            "https://access.redhat.com/articles/11258"
        )

        return solution


    def get_references(self) -> str:
        return (
            f"https://access.redhat.com/security/updates/classification/#"
            f"{self.errata.security_impact.lower()}"
        )


    def update_doc(self) -> bool:
        problem_description = self.get_problem_description()
        if problem_description == "":
            logger.info(f"Skipping {self.errata.errata_id}: missing CVE flaw bugs and/or CVE Names!")
            return False

        data = {
            "advisory[synopsis]": self.get_synopsis(),
            "advisory[topic]": self.get_topic(),
            "advisory[description]": problem_description, #self.get_problem_description(),
            "advisory[solution]": self.get_solution(),
            "advisory[reference]": self.get_references(),
        }

        logger.info("ADVISORY DATA:")
        logger.info(pformat(data, width=100))
        if self.readonly:
            logger.info("READONLY: skipping doc update")
            return True

        url = ADVISORY_URL.format(id=self.errata.errata_id) 
        r = ErrataConnector()._put(
            #u=f"{ADVISORY_URL_ET}{self.errata.errata_id}", data=data
            u=url, data=data
        )
        ErrataConnector()._processResponse(r)
        logger.info(f"STATUS - UPLOAD DATA: {r.status_code}")
        
        # another method - direct API call
        #post_res = self.put_advisory_info(data)
        #logger.info(f"STATUS - UPLOAD DATA: {post_res.status_code}")

        return True


    def approve_doc(self):
        if self.readonly:
            logger.info("READONLY: skipping doc approval")
            return
        
        url = ADVISORY_URL.format(id=self.errata.errata_id)
        r = ErrataConnector()._put(
            #u=f"{ADVISORY_URL_ET}{self.errata.errata_id}",
            u=url,
            data={"advisory[doc_complete]": 1},
        )
        ErrataConnector()._processResponse(r)
        logger.info(f"STATUS - APPROVE DOCS: {r.status_code}")

        # another method - direct API call
        #data = {"advisory[doc_complete]": "1"}
        #post_res = self.put_advisory_info(data) # status code: 204 (No Content - no body)
        #logger.info(f"STATUS - APPROVE DOCS: {post_res.status_code}")


    def get_package_name(self, nvr) -> str:
        """Returns a package name when provided an full nvr"""
        return self.get_build(nvr)["package_name"]


    def get_build(self, nvr) -> dict:
        build = self.brew_session.getBuild(nvr)

        if not build:
            raise AttributeError(f"'{nvr}' not found in brew.")
        return build


    # main method 
    def process_rhsa_advisories(self):

        for id in self.errata_ids:
 
            # 1) Erratum object
            e = Erratum(errata_id=id) # e.g. 133231
            self.errata = e
    
            # 2) list of CVE Names
            if e.cve_names != None:
                self.errata_cve_names = e.cve_names.split(' ')

            errata_builds = set(
                [
                    item
                    for sublist in [x for _, x in e.errata_builds.items()]
                    for item in sublist
                ]
            )

            logger.info(f"SYNOPSIS: {e.synopsis}")
            logger.info(f"ADVISORY URL: {DETAILS_ADVISORY_URL}{e.errata_id}")
            logger.info(f"LIST OF BUILDS: {errata_builds}")

            if len(errata_builds) > 1:
                logger.info(f"Skipping multiple-build errata {e.errata_id}")
                logger.info('='*50)
                continue
            if not len(errata_builds):
                logger.info(f"Skipping errata without attached build {e.errata_id}")
                logger.info('='*50)
                continue


            url = ADVISORY_URL.format(id=id) 
            self.errata_json = ErrataConnector()._get(url)
            

            # 3) package name       
            self.errata_package = self.get_package_name(errata_builds.pop())
            logger.info(f"ERRATA PKG: {self.errata_package}")
            
            # ------------------------ 
            # 4) send data to server
            if not self.update_doc():
                logger.info('='*50)
                continue

            # 5) approve docs
            self.approve_doc()

            # 6) clean lists
            self.errata_flaws = list()
            self.errata_cve_names = list()

            # TODO: not needed in case of RHSAs, might be implemented later
            self.errata_bugfixes = list()
            self.errata_enhancements = list()
            
            logger.info('-'*50)

    
    def prepare_mail(self, err_msg: str) -> (str, list, str):
        text = (
            f"Hello,\n\n"
            f"The advisory {DETAILS_ADVISORY_URL}{self.errata.errata_id} has the following issue:\n\n"
            f"{err_msg}\n\n"
            f"The advisory has to contain the same number of CV flaw bugs (at least 1) and corresponding CVE Names. An advisory without CVE flaw bugs or CVE Names is incomplete. Customers might not get the information which CVEs are being fixed.\n"
            f"Can you please fix this issue ASAP?\n\n"
            f"Thank you very much in advance.\nKind regards\n"
        )

        receivers = list()
        receivers.append(self.errata.package_owner_email)
        receivers.append(self.errata.qe_email)

        subject = f"RHSA request for update {self.errata.errata_id}"

        return subject, receivers, text

    
    def send_email(self,
        text: str,
        receivers: list,
        subject: str,
        sender=SENDER_EMAILS,
        smtp_server=SMTP_SERVER
    ):
        if self.readonly:
            logger.info(f"EMAIL: We would send a mail to {receivers}")
            logger.info(f"EMAIL: {subject}")
            logger.info(f"EMAIL: {text}")
            return

        logger.debug("EMAIL: Sending email")

        receivers = [x for x in receivers if x]
        msg = MIMEMultipart()
        msg["From"] = sender
        msg["To"] = ", ".join(set(receivers))
        msg["Cc"] = ", ".join(set(DOCS_EMAILS))
        msg["Subject"] = subject
        msg.attach(MIMEText(text))

        all_receivers = receivers + DOCS_EMAILS

        smtp = SMTP(SMTP_SERVER)
        logger.info(f"EMAIL: Sending email to {all_receivers}")
        logger.info(f"EMAIL: {msg}")
        
        smtp.sendmail(from_addr=sender, to_addrs=all_receivers, msg=msg.as_string())
        logger.info(f"EMAIL: Sent")
        smtp.close()


    def check_cve_bzs_names(self) -> bool:
        error = False

        if len(self.errata_cve_names) != len(self.errata_flaws):
            logger.error(f"Advisory URL: {DETAILS_ADVISORY_URL}{self.errata.errata_id}")
            err_msg = f"Different number of CVE flaw bugs: {len(self.errata_flaws)} and CVE Names: {len(self.errata_cve_names)}"
            logger.error(f"{err_msg}! You can contact:\n* Package Owner: {self.errata.package_owner_email}\n* QE: {self.errata.qe_email}")
            
            error = True

        if len(self.errata_cve_names) == 0 and len(self.errata_flaws) == 0:
            logger.error(f"Advisory URL: {DETAILS_ADVISORY_URL}{self.errata.errata_id}")
            err_msg = f"Empty CVE flaw bugs and CVE Names"
            logger.error(f"{err_msg}! You can contact:\n* Package Owner: {self.errata.package_owner_email}\n* QE: {self.errata.qe_email}")
            
            error = True

        if  len(self.errata_cve_names) == len(self.errata_flaws):  
            #logger.info(f"CVE NAMES: {self.errata_cve_names}")
            #logger.info(f"CVE flaw bugs: {self.errata_flaws}") 
            
            for item in self.errata_flaws:
                for alias in item.get('alias', []):
                    if alias not in self.errata_cve_names:
                        logger.error(f"Advisory URL: {DETAILS_ADVISORY_URL}{self.errata.errata_id}")
                        err_msg = f"Different CVE flaw bugs and CVE Names"
                        logger.error(f"{err_msg}! You can contact:\n* Package Owner: {self.errata.package_owner_email}\n* QE: {self.errata.qe_email}")
                        error = True

        if error:
            if self.email:
                subject, receivers, text = self.prepare_mail(err_msg)
                self.send_email(text, receivers, subject)
            return False

        return True


    # Inspired by: https://issues.redhat.com/browse/OSCI-6533 (errata-text.py)
    def load_bugs(self) -> bool:

        # TODO: Is this flag needed? It is not used anywhere else 
        rebase = False

        for bug in self.errata_json['bugs']['bugs']:
            bug = bug['bug']

            #logger.info(f"ALL BUGS: {self.errata_json['bugs']['bugs']}")
            #logger.info(f"ALL ERRATA DATA: {self.errata_json}")

            # pre-processing
            for i in ( 'alias', 'flags', 'keywords'):
                bug[i] = re.split(r', *', bug[i])

            # Rebase kw can be set on SecurityTracking bugs that are ignored
            if 'Rebase' in bug['keywords']:
                rebase = True

            # SecurityTracking are not to be mentioned in text
            if 'SecurityTracking' in bug['keywords']:
                continue

            # ignore requires_doc_text- bugs
            if 'requires_doc_text-' in bug['flags']:
                continue

            # sort bugs by type
            # 1520 == vulnerability - package name not in ET API data
            # https://errata.devel.redhat.com/api/v1/packages/vulnerability
            if bug['product'] == 'Security Response' and bug['package_id'] == 1520:
                # skip placeholders
                if 'Tracking' in bug['keywords']:
                    continue

                # remove non-CVE aliases
                bug['alias'] = [ x for x in bug['alias'] if RE_CVE.match(x) ]
                self.errata_flaws.append(bug)
                self.empty_bugs = False
            elif 'FutureFeature' in bug['keywords']:
                self.errata_enhancements.append(bug)
                self.empty_bugs = False
            else:
                self.errata_bugfixes.append(bug)
                self.empty_bugs = False

        # check if Bugzilla Bugs (CVEs) match CVE Names
        if not self.check_cve_bzs_names():
            return False

        return True       

    # flaw key function - to determine ordering
    # returns tuple with values for: impact/priority, csaw status, cve year, cve
    # number, bug id
    def flaw_key(self, bug: dict) -> tuple:
        bug_prios = [ 'urgent', 'high', 'medium', 'low', 'unspecified' ]

        if 'hightouch+' in bug['flags']:
            csaw_status = 0
        elif 'hightouch-lite+' in bug['flags']:
            csaw_status = 1
        else:
            csaw_status = 2

        if len(bug['alias']) == 0:
            # this should not normally happen
            cve_year = 99999
            cve_id = 0
        else:
            cve_year, cve_id = [ int(x) for x in bug['alias'][0].split('-')[1:] ]
        
        return ( bug_prios.index(bug['priority']), csaw_status, cve_year, cve_id, bug['id'] )


    def format_security_description(self, bug: dict) -> str:
        match = RE_FLAW_SUMMARY.match(bug['short_desc'])
        if not match:
            raise Exception('Malformed summary in flaw {}: {}'
                .format(bug['id'], bug['short_desc']))

        return '* {} ({})'.format(match.group(1), ', '.join(bug['alias']))


    def format_nonsecurity_description(self, bug: dict) -> str:
        description = bug['short_desc']
        description = re.sub(r" ?\[ZStream Clone\]$", "", description)
        description = re.sub(r" ?\[rhel-[0-9]+.[0-9]+(.[0-9]+)?.z\]$", "", description)
        description = re.sub(r" ?\[rhscl-[0-9]+.[0-9]+.z\]$", "", description)

        return '* {} (BZ#{})'.format(description, bug['id'])


    def generate_bug_list(self) -> str:
        buglist = ""
        if len(self.errata_flaws) > 0:
            buglist += "Security Fix(es):\n\n"

            for bug in self.errata_flaws:
                buglist += f"{self.format_security_description(bug)}\n\n"

            buglist += "For more details about the security issue(s), including the impact, a CVSS score, acknowledgments, and other related information, refer to the CVE page(s) listed in the References section.\n"

        if len(self.errata_bugfixes) > 0:
            buglist += "Bug Fix(es):\n\n"

            for bug in self.errata_bugfixes:
                buglist += f"{self.format_nonsecurity_description(bug)}\n\n"

        if len(self.errata_enhancements) > 0:
            buglist += "Enhancement(s):\n\n"

            for bug in self.errata_enhancements:
                buglist += f"{self.format_nonsecurity_description(bug)}\n\n"

        return buglist


# ------------------------------
def main(): 

    args = get_args()

    set_logger(args.level)

    errata_updater = ErrataUpdater()
    
    # only one advisory
    if args.advisory:
        errata_updater.errata_ids = [args.advisory]

    # processing RHSA filter
    else:
        errata_updater.errata_ids = errata_updater.get_rhsa_advisories()
    
    # readonly=True prevents sending data or emails  
    if args.readonly:
        errata_updater.readonly = True
        logger.info(f"ARGS: READONLY = ON")
    else:
        errata_updater.readonly = False
        logger.info(f"ARGS: READONLY = OFF")

    # email=True ensures to send email to PO and QA when error occurs
    if args.email:
        errata_updater.email = True
        logger.info(f"ARGS: EMAIL = ON")

    else:
        errata_updater.email = False
        logger.info(f"ARGS: EMAIL = OFF")

    logger.info(f"ADVISORY IDs:\n{errata_updater.errata_ids}")
    logger.info(f"NUMBER OF ADVISORIES: {len(errata_updater.errata_ids)}")  
    logger.info('='*50)  

    errata_updater.process_rhsa_advisories()

    logger.info('='*50)
    

if __name__ == "__main__":
    main()

