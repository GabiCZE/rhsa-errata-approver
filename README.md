# RHSA-errata-approver

## Description

This script generates documentation texts for given RHSA advisories specified by URL filter for RHSA or by Advisory ID. 
If the CVE BZ or CVE names for a given advisory are missing or their number does not match, the script sends an email to the PO and QA.
In documentation process, we modify these fields: Synopsis, Topic, Problem Description, Solution and References.

NOTE: This work is based on the errata-text.py script provided in https://issues.redhat.com/browse/OSCI-6533
and also errata-automation project: https://gitlab.cee.redhat.com/osci/errata-automation.    


## Installation

```console
pip install errata-tool requests-kerberos koji
```

- [errata-tool](https://pypi.org/project/errata-tool/): Python API to Red Hat's Errata Tool (version 1.32.0)
- [requests-kerberos](https://pypi.org/project/requests-kerberos/): Kerberos/GSSAPI authentication (version 0.15.0)
- [koji](https://pypi.org/project/koji/): An RPM-based build system (version 1.34.2)


## Usage

```console
./update-rhsa-doc.py [-h] [--advisory ADVISORY] [--readonly] [-l LEVEL] [-e]

Options:
  -h, --help               Show this help message and exit
  --advisory ADVISORY      ID of the advisory
  --readonly               Set the script to readonly mode
  -l LEVEL, --level LEVEL  Set logging level: 'error', 'warning' (default), 'info', 'debug'
  -e, --email              Send email to PO and QA if error occurs
```

## Examples

**Example 1**: Approve the `https://errata.devel.redhat.com/advisory/details/132654` advisory. 
```console
$ ./update_rhsa_doc.py --advisory 132654 -l 'info' -e

INFO: ARGS: READONLY = ON
INFO: ADVISORY IDs:
[132654]
INFO: NUMBER OF ADVISORIES: 1
INFO: ==================================================
INFO: SYNOPSIS: Important: 389-ds:1.4 security and bug fix update
INFO: ADVISORY URL: https://errata.devel.redhat.com/advisory/details/132654
INFO: LIST OF BUILDS: {'389-ds-1.4-8100020240613122040.25e700aa'}
INFO: ERRATA PKG: 389-ds
INFO: PACKAGE URL: https://errata.devel.redhat.com/api/v1/packages/389-ds
INFO: ADVISORY DATA:
INFO: {'advisory[description]': '389 Directory Server is an LDAP version 3 (LDAPv3) compliant server. '
                          'The base packages include the Lightweight Directory Access Protocol '
                          '(LDAP) server and command-line utilities for server administration. \n'
                          '\n'
                          'Security Fix(es):\n'
                          '\n'
                          '* 389-ds-base: potential denial of service via specially crafted '
                          'kerberos AS-REQ request (CVE-2024-3657)\n'
                          '\n'
                          '* 389-ds-base: Malformed userPassword may cause crash at do_modify in '
                          'slapd/modify.c (CVE-2024-2199)\n'
                          '\n'
                          'For more details about the security issue(s), including the impact, a '
                          'CVSS score, acknowledgments, and other related information, refer to '
                          'the CVE page(s) listed in the References section.\n',
 'advisory[reference]': 'https://access.redhat.com/security/updates/classification/#important',
 'advisory[solution]': 'For details on how to apply this update, which includes the changes '
                       'described in this advisory, refer to:\n'
                       '\n'
                       'https://access.redhat.com/articles/11258',
 'advisory[synopsis]': 'Important: 389-ds security update',
 'advisory[topic]': 'An update for 389-ds is now available for Red Hat Enterprise Linux 8.\n'
                    '\n'
                    'Red Hat Product Security has rated this update as having a security impact of '
                    'Important. A Common Vulnerability Scoring System (CVSS) base score, which '
                    'gives a detailed severity rating, is available for each vulnerability from '
                    'the CVE link(s) in the References section.'}
INFO: STATUS - UPLOAD DATA: 204
INFO: STATUS - APPROVE DOCS: 204

```

**Example 2**: Approve all advisories in the [RHSA filter](https://errata.devel.redhat.com/api/v1/erratum/search?doc_status%5B%5D=not_reqd&doc_status%5B%5D=requested&doc_status%5B%5D=need_redraft&hotfix_option=&prerelease_option=&product%5B%5D=16&product%5B%5D=102&show_state_REL_PREP=1&show_type_RHSA=1&sort_by_fields%5B%5D=reldate&sort_by_fields%5B%5D=new&page=1):  
Note that the output is shortened. 

```console
$ ./update_rhsa_doc.py -l 'info' -e

INFO: NUMBER OF ADVISORIES: 38
INFO: ARGS: READONLY = ON
INFO: ADVISORY IDs:
[129732, 133323, 130945, 129966, 128825, 133519, 133515, 133511, 133468, 133518, 133514, 133395, 133327, 133326, 133206, 132986, 132738, 132654, 131781, 131301, 130781, 130024, 132926, 132887, 132247, 132245, 132243, 132944, 130850, 133512, 133292, 132941, 133217, 131737, 130912, 130635, 129914, 129124]
INFO: NUMBER OF ADVISORIES: 38
INFO: ==================================================
INFO: SYNOPSIS: Important: CVE-2023-4727 for pki-core
INFO: ADVISORY URL: https://errata.devel.redhat.com/advisory/details/129732
INFO: LIST OF BUILDS: {'pki-core-10.6-8080020240329143735.693a3987'}
INFO: ERRATA PKG: pki-core
INFO: PACKAGE URL: https://errata.devel.redhat.com/api/v1/packages/pki-core
ERROR: Different number of CVE Bugs: 0 and CVE Names: 1! You can contact:
* Package Owner: mfargett@redhat.com
* QE: skhandel@redhat.com
INFO: EMAIL: Sending email to ['mfargett@redhat.com', 'skhandel@redhat.com', 'gnecasov@redhat.com', 'lspackova@redhat.com']
INFO: EMAIL: Content-Type: multipart/mixed; boundary="===============5311578189425318532=="
MIME-Version: 1.0
From: gnecasov@redhat.com
To: mfargett@redhat.com, skhandel@redhat.com
Cc: gnecasov@redhat.com, lspackova@redhat.com
Subject: RHSA request for update 129732

--===============5311578189425318532==
Content-Type: text/plain; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit

Hello,

The advisory https://errata.devel.redhat.com/advisory/details/129732 has the following issue:

Different number of CVE flaw bugs: 0 and CVE Names: 1

The advisory has to contain the same number of CV flaw bugs (at least 1) and corresponding CVE Names. An advisory without CVE flaw bugs or CVE Names is incomplete. Customers might not get the information which CVEs are being fixed.
Can you please fix this issue ASAP?

Thank you very much in advance.
Kind regards

--===============5311578189425318532==--
INFO: EMAIL: Sent
INFO: Skipping 129732: missing CVE BZs and/or CVE Names!
INFO: ==================================================
INFO: SYNOPSIS: Important: python3.11 security update
INFO: ADVISORY URL: https://errata.devel.redhat.com/advisory/details/133323
INFO: LIST OF BUILDS: {'python3.11-3.11.9-1.el8_10'}
INFO: ERRATA PKG: python3.11
INFO: PACKAGE URL: https://errata.devel.redhat.com/api/v1/packages/python3
INFO: ADVISORY DATA:
INFO: {'advisory[description]': 'Python is an interpreted, interactive, object-oriented programming '
                          'language, which includes modules, classes, exceptions, very high level '
                          'dynamic data types and dynamic typing. Python supports interfaces to '
                          'many system calls and libraries, as well as to various windowing '
                          'systems.\n'
                          '\n'
                          'Security Fix(es):\n'
                          '\n'
                          '* python: Path traversal on tempfile.TemporaryDirectory '
                          '(CVE-2023-6597)\n'
                          '\n'
                          '* python: The zipfile module is vulnerable to zip-bombs leading to '
                          'denial of service (CVE-2024-0450)\n'
                          '\n'
                          'For more details about the security issue(s), including the impact, a '
                          'CVSS score, acknowledgments, and other related information, refer to '
                          'the CVE page(s) listed in the References section.\n',
 'advisory[reference]': 'https://access.redhat.com/security/updates/classification/#important',
 'advisory[solution]': 'For details on how to apply this update, which includes the changes '
                       'described in this advisory, refer to:\n'
                       '\n'
                       'https://access.redhat.com/articles/11258',
 'advisory[synopsis]': 'Important: python3 security update',
 'advisory[topic]': 'An update for python3 is now available for Red Hat Enterprise Linux 8.\n'
                    '\n'
                    'Red Hat Product Security has rated this update as having a security impact of '
                    'Important. A Common Vulnerability Scoring System (CVSS) base score, which '
                    'gives a detailed severity rating, is available for each vulnerability from '
                    'the CVE link(s) in the References section.'}
INFO: STATUS - UPLOAD DATA: 204
INFO: STATUS - APPROVE DOCS: 204
...
```

## Notes

* JIRA tracker: https://issues.redhat.com/browse/RHELWF-10849
* The `[-e]` email option is not mandatory because it is a temporary solution until Security Approvals are fixed. 


## Support
Gabriela Nečasová, gnecasov@redhat.com, Slack: Gabi N. 


